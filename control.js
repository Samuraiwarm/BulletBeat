function control(){ //68 = right, 65 = left, 87 = up, 83 = down
	if(keyIsDown(SHIFT)){
		if (keyIsDown(68) && !keyIsDown(65) && !keyIsDown(87) && !keyIsDown(83) && p.x <= 590){
			p.move(2.5,0); //right
		}
		if (keyIsDown(65) && !keyIsDown(68) && !keyIsDown(87) && !keyIsDown(83) && p.x >= 10){
			p.move(-2.5,0); //left
		}
		if (keyIsDown(87) && !keyIsDown(83) && !keyIsDown(68) && !keyIsDown(65) && p.y >= 10){
			p.move(0,-2.5); //up
		}
		if (keyIsDown(83) && !keyIsDown(87) && !keyIsDown(68) && !keyIsDown(65) && p.y <= 590){
			p.move(0,2.5); //down
		}
		if (keyIsDown(87) && keyIsDown(65) && !keyIsDown(83) && !keyIsDown(68) && p.y >= 10 && p.x >= 10){
			p.move(-2.5*Math.sqrt(2)/2,-2.5*Math.sqrt(2)/2); //upleft
		}
		if (keyIsDown(87) && keyIsDown(68) && !keyIsDown(83) && !keyIsDown(65) && p.y >= 10 && p.x <= 590){
			p.move(2.5*Math.sqrt(2)/2,-2.5*Math.sqrt(2)/2); //upright
		}
		if (keyIsDown(83) && keyIsDown(65) && !keyIsDown(87) && !keyIsDown(68) && p.y <= 590 && p.x >= 10){
			p.move(-2.5*Math.sqrt(2)/2,2.5*Math.sqrt(2)/2); //downleft
		}
		if (keyIsDown(83) && keyIsDown(68) && !keyIsDown(87) && !keyIsDown(65) && p.y <= 590 && p.x <= 590){
			p.move(2.5*Math.sqrt(2)/2,2.5*Math.sqrt(2)/2); //downright
		}
		if (keyIsDown(68) && !keyIsDown(65) && keyIsDown(87) && keyIsDown(83) && p.x <= 590){
			p.move(2.5,0);
		}
		if (keyIsDown(65) && !keyIsDown(68) && keyIsDown(87) && keyIsDown(83) && p.x >= 10){
			p.move(-2.5,0);
		}
		if (keyIsDown(87) && !keyIsDown(83) && keyIsDown(68) && keyIsDown(65) && p.y >= 10){
			p.move(0,-2.5);
		}
		if (keyIsDown(83) && !keyIsDown(87) && keyIsDown(68) && keyIsDown(65) && p.y <= 590){
			p.move(0,2.5);
		}
	} else {
		if (keyIsDown(68) && !keyIsDown(65) && !keyIsDown(87) && !keyIsDown(83) && p.x <= 590){
			p.move(5,0); //right
		}
		if (keyIsDown(65) && !keyIsDown(68) && !keyIsDown(87) && !keyIsDown(83) && p.x >= 10){
			p.move(-5,0); //left
		}
		if (keyIsDown(87) && !keyIsDown(83) && !keyIsDown(68) && !keyIsDown(65) && p.y >= 10){
			p.move(0,-5); //up
		}
		if (keyIsDown(83) && !keyIsDown(87) && !keyIsDown(68) && !keyIsDown(65) && p.y <= 590){
			p.move(0,5); //down
		}
		if (keyIsDown(87) && keyIsDown(65) && !keyIsDown(83) && !keyIsDown(68) && p.y >= 10 && p.x >= 10){
			p.move(-5*Math.sqrt(2)/2,-5*Math.sqrt(2)/2); //upleft
		}
		if (keyIsDown(87) && keyIsDown(68) && !keyIsDown(83) && !keyIsDown(65) && p.y >= 10 && p.x <= 590){
			p.move(5*Math.sqrt(2)/2,-5*Math.sqrt(2)/2); //upright
		}
		if (keyIsDown(83) && keyIsDown(65) && !keyIsDown(87) && !keyIsDown(68) && p.y <= 590 && p.x >= 10){
			p.move(-5*Math.sqrt(2)/2,5*Math.sqrt(2)/2); //downleft
		}
		if (keyIsDown(83) && keyIsDown(68) && !keyIsDown(87) && !keyIsDown(65) && p.y <= 590 && p.x <= 590){
			p.move(5*Math.sqrt(2)/2,5*Math.sqrt(2)/2); //downright
		}
		if (keyIsDown(68) && !keyIsDown(65) && keyIsDown(87) && keyIsDown(83) && p.x <= 590){
			p.move(5,0);
		}
		if (keyIsDown(65) && !keyIsDown(68) && keyIsDown(87) && keyIsDown(83) && p.x >= 10){
			p.move(-5,0);
		}
		if (keyIsDown(87) && !keyIsDown(83) && keyIsDown(68) && keyIsDown(65) && p.y >= 10){
			p.move(0,-5);
		}
		if (keyIsDown(83) && !keyIsDown(87) && keyIsDown(68) && keyIsDown(65) && p.y <= 590){
			p.move(0,5);
		}
	}
	// }
	// if (keyIsDown(RIGHT_ARROW) && !keyIsDown(LEFT_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(DOWN_ARROW) && p.x <= 590){
	// 	p.move(5,0);
	// }
	// if (keyIsDown(LEFT_ARROW) && !keyIsDown(RIGHT_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(DOWN_ARROW) && p.x >= 10){
	// 	p.move(-5,0);
	// }
	// if (keyIsDown(UP_ARROW) && !keyIsDown(DOWN_ARROW) && !keyIsDown(RIGHT_ARROW) && !keyIsDown(LEFT_ARROW) && p.y >= 10){
	// 	p.move(0,-5);
	// }
	// if (keyIsDown(DOWN_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(RIGHT_ARROW) && !keyIsDown(LEFT_ARROW) && p.y <= 590){
	// 	p.move(0,5);
	// }
	// if (keyIsDown(UP_ARROW) && keyIsDown(LEFT_ARROW) && !keyIsDown(DOWN_ARROW) && !keyIsDown(RIGHT_ARROW) && p.y >= 10 && p.x >= 10){
	// 	p.move(-5*Math.sqrt(2)/2,-5*Math.sqrt(2)/2);
	// }
	// if (keyIsDown(UP_ARROW) && keyIsDown(RIGHT_ARROW) && !keyIsDown(DOWN_ARROW) && !keyIsDown(LEFT_ARROW) && p.y >= 10 && p.x <= 590){
	// 	p.move(5*Math.sqrt(2)/2,-5*Math.sqrt(2)/2);
	// }
	// if (keyIsDown(DOWN_ARROW) && keyIsDown(LEFT_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(RIGHT_ARROW) && p.y <= 590 && p.x >= 10){
	// 	p.move(-5*Math.sqrt(2)/2,5*Math.sqrt(2)/2);
	// }
	// if (keyIsDown(DOWN_ARROW) && keyIsDown(RIGHT_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(LEFT_ARROW) && p.y <= 590 && p.x <= 590){
	// 	p.move(5*Math.sqrt(2)/2,5*Math.sqrt(2)/2);
	// }
	// if (keyIsDown(RIGHT_ARROW) && !keyIsDown(LEFT_ARROW) && keyIsDown(UP_ARROW) && keyIsDown(DOWN_ARROW) && p.x <= 590){
	// 	p.move(5,0);
	// }
	// if (keyIsDown(LEFT_ARROW) && !keyIsDown(RIGHT_ARROW) && keyIsDown(UP_ARROW) && keyIsDown(DOWN_ARROW) && p.x >= 10){
	// 	p.move(-5,0);
	// }
	// if (keyIsDown(UP_ARROW) && !keyIsDown(DOWN_ARROW) && keyIsDown(RIGHT_ARROW) && keyIsDown(LEFT_ARROW) && p.y >= 10){
	// 	p.move(0,-5);
	// }
	// if (keyIsDown(DOWN_ARROW) && !keyIsDown(UP_ARROW) && keyIsDown(RIGHT_ARROW) && keyIsDown(LEFT_ARROW) && p.y <= 590){
	// 	p.move(0,5);
 //    }
}

//alternative arrow keys
// 	if (keyIsDown(RIGHT_ARROW) && !keyIsDown(LEFT_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(DOWN_ARROW) && p.x <= 590){
// 		p.move(5,0);
// 	}
// 	if (keyIsDown(LEFT_ARROW) && !keyIsDown(RIGHT_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(DOWN_ARROW) && p.x >= 10){
// 		p.move(-5,0);
// 	}
// 	if (keyIsDown(UP_ARROW) && !keyIsDown(DOWN_ARROW) && !keyIsDown(RIGHT_ARROW) && !keyIsDown(LEFT_ARROW) && p.y >= 10){
// 		p.move(0,-5);
// 	}
// 	if (keyIsDown(DOWN_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(RIGHT_ARROW) && !keyIsDown(LEFT_ARROW) && p.y <= 590){
// 		p.move(0,5);
// 	}
// 	if (keyIsDown(UP_ARROW) && keyIsDown(LEFT_ARROW) && !keyIsDown(DOWN_ARROW) && !keyIsDown(RIGHT_ARROW) && p.y >= 10 && p.x >= 10){
// 		p.move(-5*Math.sqrt(2)/2,-5*Math.sqrt(2)/2);
// 	}
// 	if (keyIsDown(UP_ARROW) && keyIsDown(RIGHT_ARROW) && !keyIsDown(DOWN_ARROW) && !keyIsDown(LEFT_ARROW) && p.y >= 10 && p.x <= 590){
// 		p.move(5*Math.sqrt(2)/2,-5*Math.sqrt(2)/2);
// 	}
// 	if (keyIsDown(DOWN_ARROW) && keyIsDown(LEFT_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(RIGHT_ARROW) && p.y <= 590 && p.x >= 10){
// 		p.move(-5*Math.sqrt(2)/2,5*Math.sqrt(2)/2);
// 	}
// 	if (keyIsDown(DOWN_ARROW) && keyIsDown(RIGHT_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(LEFT_ARROW) && p.y <= 590 && p.x <= 590){
// 		p.move(5*Math.sqrt(2)/2,5*Math.sqrt(2)/2);
// 	}
// 	if (keyIsDown(RIGHT_ARROW) && !keyIsDown(LEFT_ARROW) && keyIsDown(UP_ARROW) && keyIsDown(DOWN_ARROW) && p.x <= 590){
// 		p.move(5,0);
// 	}
// 	if (keyIsDown(LEFT_ARROW) && !keyIsDown(RIGHT_ARROW) && keyIsDown(UP_ARROW) && keyIsDown(DOWN_ARROW) && p.x >= 10){
// 		p.move(-5,0);
// 	}
// 	if (keyIsDown(UP_ARROW) && !keyIsDown(DOWN_ARROW) && keyIsDown(RIGHT_ARROW) && keyIsDown(LEFT_ARROW) && p.y >= 10){
// 		p.move(0,-5);
// 	}
// 	if (keyIsDown(DOWN_ARROW) && !keyIsDown(UP_ARROW) && keyIsDown(RIGHT_ARROW) && keyIsDown(LEFT_ARROW) && p.y <= 590){
// 		p.move(0,5);
//     }
// }