function Bullet(radius,
				theta,
				bulletSpeed,
				bulletAccel,
				angleSpeed,
				radiusIncrement,
				timeDelay){
	this.x = 300;
	this.y = 300;
	this.radius = radius;
	this.angle = theta;
	this.bulletSpeed = bulletSpeed;
	this.bulletAccel = bulletAccel;
	this.angleSpeed = angleSpeed;
	this.radiusIncrement = radiusIncrement;
	this.timeDelay = timeDelay;
	this.move = function(){
		if(frameCount*1000/fps>=this.timeDelay){
			if(this.x+Math.abs(radius)/2 > 0 && this.x-Math.abs(radius)/2 < 600
			   && this.y+Math.abs(radius)/2 > 0 && this.y-Math.abs(radius)/2 < 600){
				this.x += bulletSpeed*Math.cos(theta*Math.PI/180);
				this.y -= bulletSpeed*Math.sin(theta*Math.PI/180);
			} else {
				bulletSpeed = 0;
				bulletAccel = 0;
				angleSpeed = 0;
				radiusIncrement = 0;
			}
		}
	}
	this.changeRadius = function(){ //changing radius over time
		if(frameCount*1000/fps>=this.timeDelay){
			radius += radiusIncrement;
		}
	}
	this.changeAngle = function(){
		if(frameCount*1000/fps>=this.timeDelay){
			theta += angleSpeed;
		}
	}
	this.changeSpeed = function(newSpeed){ //changing speed
		if(frameCount*1000/fps>=this.timeDelay){
			bulletSpeed = newSpeed;
		}
	}
	this.accelSpeed = function(){
		if(frameCount*1000/fps>=this.timeDelay){
			bulletSpeed += bulletAccel;
		}
	}
	this.isHit = function(){
		return (Math.hypot(this.x-p.x,this.y-p.y)<2.5+Math.abs(radius)/2
			    && Math.hypot(e.x-this.x,e.y-this.y)>15+Math.abs(radius)/2
			    && this.isAlive());
	}
	this.show = function(){
		if(Math.hypot(e.x-this.x,e.y-this.y)!=0 && this.isAlive()){
			fill(255);
			ellipse(this.x,this.y,Math.abs(radius),Math.abs(radius));
		}
	}
	this.isAlive = function(){
		return this.x+Math.abs(radius)/2 > 0 && this.x-Math.abs(radius)/2 < 600
		   && this.y+Math.abs(radius)/2 > 0 && this.y-Math.abs(radius)/2 < 600
	}
	this.render = function(){
		translate(this.x,this.y);
	}
}