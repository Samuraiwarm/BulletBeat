function Player(){
	this.x = 300;
	this.y = 450;
	this.show = function(){
		fill(255,127,0,255);
		ellipse(this.x,this.y,10,10);
	}
	this.move = function(dx,dy){
		this.x += dx;
		this.y += dy;
	}
	this.render = function(){
		translate(this.x,this.y);
	}
}
function Enemy(){
	this.x = 300;
	this.y = 300;
	this.show = function(){
		fill(255,0,0,255);
		ellipse(this.x,this.y,10,10);
	}
	this.isHit = function(){
		return (Math.hypot(e.x-p.x,e.y-p.y)<7.5);
	}
}