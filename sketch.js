function preload(){
	soundFormats("mp3");
	gengaozo = loadSound("Gengaozo/gengaozo.mp3");
}

function setup(){
	createCanvas(600,600);
	frameRate(fps);
	gengaozo.setVolume(0.1);
	gengaozo.play();
	for(var i=0;i<bulletCount;i++){
		var b = new Bullet(bulletData[i][0],
						   bulletData[i][1],
						   bulletData[i][2],
						   bulletData[i][3],
						   bulletData[i][4],
						   bulletData[i][5],
						   bulletData[i][6]);
		bulletPool.push(b);
	}
}


function draw(){
	control();
	background(0);
	p.show();
	e.show();

	for(var i=0; i<bulletCount; i++){
		bulletPool[i].accelSpeed();
		bulletPool[i].changeAngle();
		bulletPool[i].changeRadius();
	}
	for(var i=0; i<bulletCount; i++){
		bulletPool[i].show();
	}
	for(var i=0; i<bulletCount; i++){
		bulletPool[i].render();
		bulletPool[i].move();
	}
	for(var i=0; i<bulletPool.length; i++){
		if(bulletPool[i].isHit()){
			console.log("itai");
			itaiCount++;
		}
	}
	if(e.isHit()){
		console.log("itai");
		itaiCount++;
	}
	p.render();
}

