var p = new Player();
var e = new Enemy();

// radius,theta,bulletSpeed,bulletAccel,angleSpeed,radiusIncrement,timeDelay
var bulletData = [
	[10,135,1,0.05,-3,0.02,500],
	[5,45,2,0,0,0,500],
	[20,30,0.5,0.05,2,0.1,500],
	[10,45,2,0,0,0,1593],
	[10,90,2,0,0,0,1593],
	[10,45,2,0,0,0,1789],
	[10,90,2,0,0,0,1789],
	[10,45,2,0,0,0,1887],
	[10,90,2,0,0,0,1887],
	[10,135,2,0,0,0,1985],
	[10,45,2,0,0,0,1985],
	[10,45,2,0,0,0,2181],
	[10,270,2,0,0,0,2181],
	[10,45,2,0,0,0,2279],
	[10,45,2,0,0,0,2377],
	[10,45,2,0,0,0,2573],
	[10,45,2,0,0,0,2671],
	[10,45,2,0,0,0,2769],
	[10,45,2,0,0,0,2965],
	[10,45,2,0,0,0,3063]
];

fourSide(10,45,2,0,0,0,5000);
fourSide(10,45,1,0.02,2,0,5500);
splash(10,90,180,8,3,0,0,0,6000);
splash(10,90,180,8,3,0,0,0,7000);
splash(10,90,180,8,3,0,0,0,8000);
splash(10,90,180,8,3,0,0,0,9000);
splash(10,90,180,8,3,0,0,0,10000);
splash(10,90,180,8,3,0,0,0,11000);
splash(10,90,180,8,3,0,0,0,12000);
splash(10,90,180,8,3,0,0,0,13000);

var bullet = new Array();
var bulletPool = new Pool();
var noteIndex;


for (var i=0; i<92; i++){
	bulletPool.pool.push(new Bullet(bulletData[i][0],
									bulletData[i][1],
									bulletData[i][2],
									bulletData[i][3],
									bulletData[i][4],
									bulletData[i][5],
									bulletData[i][6],
									bulletData[i][7]))
	noteIndex++;
}


function fourSide(radius,theta,bulletSpeed,bulletAccel,angleSpeed,radiusIncrement,timeDelay){
	for (var i=0; i<4; i++){
		bulletData.push([radius,
						theta+90*i,
						bulletSpeed,
						bulletAccel,
						angleSpeed,
						radiusIncrement,
						timeDelay]);
	}
}

function splash(radius,theta1,theta2,number,bulletSpeed,bulletAccel,angleSpeed,radiusIncrement,timeDelay){
	for (var i=0; i<number; i++){
		bulletData.push([radius,
						theta1+i*(theta2-theta1)/(number-1),
						bulletSpeed,
						bulletAccel,
						angleSpeed,
						radiusIncrement,
						timeDelay]);		
	}
}

function Player(){
	this.x = 300;
	this.y = 450;
	this.show = function(){
		fill(255,127,0,255);
		ellipse(this.x,this.y,10,10);
	}
	this.move = function(dx,dy){
		this.x += dx;
		this.y += dy;
	}
	this.render = function(){
		translate(this.x,this.y);
	}
}

function Enemy(){
	this.x = 300;
	this.y = 300;
	this.show = function(){
		fill(255,0,0,255);
		ellipse(this.x,this.y,30,30);
	}
}

function Pool(){
	this.size = 50;
	this.pool = [];
	this.init = function() {
		for (var i = 0; i < size; i++) {
			var element = new Bullet(bulletData[i][0],
									bulletData[i][1],
									bulletData[i][2],
									bulletData[i][3],
									bulletData[i][4],
									bulletData[i][5],
									bulletData[i][6],
									bulletData[i][7]);
			pool[i] = element;
		}
	}
	this.push = function(){
		for (var i=0; i<bulletPool.size; i++){
			if(noteIndex<92){
				if(!bullet[i].isAlive){
					pool[i] = new Bullet(bulletData[noteIndex][0],
													 bulletData[noteIndex][1],
													 bulletData[noteIndex][2],
													 bulletData[noteIndex][3],
													 bulletData[noteIndex][4],
													 bulletData[noteIndex][5],
													 bulletData[noteIndex][6],
													 bulletData[noteIndex][7]
													 -bulletData[noteIndex-1][7]);
				}
			}
		}
	}
}

function Bullet(radius,
				theta,
				bulletSpeed,
				bulletAccel,
				angleSpeed,
				radiusIncrement,
				timeDelay){
	this.x = 300;
	this.y = 300;
	this.radius = radius;
	this.angle = theta;
	this.bulletSpeed = bulletSpeed;
	this.bulletAccel = bulletAccel;
	this.angleSpeed = angleSpeed;
	this.radiusIncrement = radiusIncrement;
	this.timeDelay = timeDelay;
	this.move = function(){
		if(this.x+radius/2 > 0 && this.x-radius/2 < 600
		   && this.y+radius/2 > 0 && this.y-radius/2 < 600){
			this.x += bulletSpeed*Math.cos(theta*Math.PI/180);
			this.y -= bulletSpeed*Math.sin(theta*Math.PI/180);
		} else {
			bulletSpeed = 0;
			bulletAccel = 0;
			angleSpeed = 0;
			radiusIncrement = 0;
			bulletPool.push();
		}
	}
	// this.init = function(bulletSpeed,theta){
	// 	this.x += speed*Math.cos(theta*Math.PI/180);
	// 	this.y -= speed*Math.sin(theta*Math.PI/180);
	// }
	this.changeRadius = function(){ //changing radius over time
		radius += radiusIncrement;
	}
	this.changeAngle = function(){
		theta += angleSpeed;
	}
	this.changeSpeed = function(newSpeed){ //changing speed
		bulletSpeed = newSpeed;
	}
	this.accelSpeed = function(){
		bulletSpeed += bulletAccel;
	}
	this.isHit = function(){
		return (Math.hypot(this.x-p.x,this.y-p.y)<2.5+radius/2
			    && Math.hypot(e.x-this.x,e.y-this.y)>15+radius/2
			    && this.isAlive());
	}
	this.show = function(){
		if(Math.hypot(e.x-this.x,e.y-this.y)>15+radius/2
		   && this.isAlive()){
			fill(255);
			ellipse(this.x,this.y,radius,radius);
		}
	}
	this.isAlive = function(){
		return Math.hypot(e.x-this.x,e.y-this.y)>15+radius/2
		   && this.x+radius/2 > 0 && this.x-radius/2 < 600
		   && this.y+radius/2 > 0 && this.y-radius/2 < 600
	}
	this.render = function(){
		translate(this.x,this.y);
	}
}

function preload(){
	soundFormats("mp3");
	gengaozo = loadSound("Gengaozo/gengaozo.mp3");
}

function myFunction() {
	var myVar = setTimeout(alertFunc, 3000);
}
function alertFunc() {
  console.log("Hello!");
}

function delayMove(i){
	setTimeout(function(){bulletPool.pool[i].move()},bulletPool.pool[i].timeDelay);
}

function delaySetup(i){
	setTimeout(function(){
		bulletPool.pool[i].accelSpeed();
		bulletPool.pool[i].changeAngle();
		bulletPool.pool[i].changeRadius();
	},bulletPool.pool[i].timeDelay);
}

function delayRender(i){
	setTimeout(function(){
		bulletPool.pool[i].show();
		bulletPool.pool[i].move();
	},bulletPool.pool[i].timeDelay);
}

function doNothing(){
}

function setup(){
	createCanvas(600,600);
	gengaozo.setVolume(0.1);
	gengaozo.play();
	myFunction();
	bulletPool.init();
}

function draw(){
	control();
	background(0);
	p.show();
	e.show();
	for (var i=0; i<bulletPool.size; i++){
		bulletPool.pool[i].show();
		delaySetup(i);
		delayMove(i);
		delayRender(i);
	}
	p.render();
	for(var i=0; i<bulletPool.size; i++){
		if(bulletPool.pool[i].isHit()){
			console.log("itai");
		}
	}
}

function control(){ //68 = right, 65 = left, 87 = up, 83 = down
	if (keyIsDown(68) && !keyIsDown(65) && !keyIsDown(87) && !keyIsDown(83) && p.x <= 590){
		p.move(5,0); //right
	}
	if (keyIsDown(65) && !keyIsDown(68) && !keyIsDown(87) && !keyIsDown(83) && p.x >= 10){
		p.move(-5,0); //left
	}
	if (keyIsDown(87) && !keyIsDown(83) && !keyIsDown(68) && !keyIsDown(65) && p.y >= 10){
		p.move(0,-5); //up
	}
	if (keyIsDown(83) && !keyIsDown(87) && !keyIsDown(68) && !keyIsDown(65) && p.y <= 590){
		p.move(0,5); //down
	}
	if (keyIsDown(87) && keyIsDown(65) && !keyIsDown(83) && !keyIsDown(68) && p.y >= 10 && p.x >= 10){
		p.move(-5*Math.sqrt(2)/2,-5*Math.sqrt(2)/2); //upleft
	}
	if (keyIsDown(87) && keyIsDown(68) && !keyIsDown(83) && !keyIsDown(65) && p.y >= 10 && p.x <= 590){
		p.move(5*Math.sqrt(2)/2,-5*Math.sqrt(2)/2); //upright
	}
	if (keyIsDown(83) && keyIsDown(65) && !keyIsDown(87) && !keyIsDown(68) && p.y <= 590 && p.x >= 10){
		p.move(-5*Math.sqrt(2)/2,5*Math.sqrt(2)/2); //downleft
	}
	if (keyIsDown(83) && keyIsDown(68) && !keyIsDown(87) && !keyIsDown(65) && p.y <= 590 && p.x <= 590){
		p.move(5*Math.sqrt(2)/2,5*Math.sqrt(2)/2); //downright
	}
	if (keyIsDown(68) && !keyIsDown(65) && keyIsDown(87) && keyIsDown(83) && p.x <= 590){
		p.move(5,0);
	}
	if (keyIsDown(65) && !keyIsDown(68) && keyIsDown(87) && keyIsDown(83) && p.x >= 10){
		p.move(-5,0);
	}
	if (keyIsDown(87) && !keyIsDown(83) && keyIsDown(68) && keyIsDown(65) && p.y >= 10){
		p.move(0,-5);
	}
	if (keyIsDown(83) && !keyIsDown(87) && keyIsDown(68) && keyIsDown(65) && p.y <= 590){
		p.move(0,5);
	}
}


//in function draw()
	// if (keyIsDown(RIGHT_ARROW) && !keyIsDown(LEFT_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(DOWN_ARROW) && p.x <= 590){
	// 	p.move(5,0);
	// }
	// if (keyIsDown(LEFT_ARROW) && !keyIsDown(RIGHT_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(DOWN_ARROW) && p.x >= 10){
	// 	p.move(-5,0);
	// }
	// if (keyIsDown(UP_ARROW) && !keyIsDown(DOWN_ARROW) && !keyIsDown(RIGHT_ARROW) && !keyIsDown(LEFT_ARROW) && p.y >= 10){
	// 	p.move(0,-5);
	// }
	// if (keyIsDown(DOWN_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(RIGHT_ARROW) && !keyIsDown(LEFT_ARROW) && p.y <= 590){
	// 	p.move(0,5);
	// }
	// if (keyIsDown(UP_ARROW) && keyIsDown(LEFT_ARROW) && !keyIsDown(DOWN_ARROW) && !keyIsDown(RIGHT_ARROW) && p.y >= 10 && p.x >= 10){
	// 	p.move(-5*Math.sqrt(2)/2,-5*Math.sqrt(2)/2);
	// }
	// if (keyIsDown(UP_ARROW) && keyIsDown(RIGHT_ARROW) && !keyIsDown(DOWN_ARROW) && !keyIsDown(LEFT_ARROW) && p.y >= 10 && p.x <= 590){
	// 	p.move(5*Math.sqrt(2)/2,-5*Math.sqrt(2)/2);
	// }
	// if (keyIsDown(DOWN_ARROW) && keyIsDown(LEFT_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(RIGHT_ARROW) && p.y <= 590 && p.x >= 10){
	// 	p.move(-5*Math.sqrt(2)/2,5*Math.sqrt(2)/2);
	// }
	// if (keyIsDown(DOWN_ARROW) && keyIsDown(RIGHT_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(LEFT_ARROW) && p.y <= 590 && p.x <= 590){
	// 	p.move(5*Math.sqrt(2)/2,5*Math.sqrt(2)/2);
	// }
	// if (keyIsDown(RIGHT_ARROW) && !keyIsDown(LEFT_ARROW) && keyIsDown(UP_ARROW) && keyIsDown(DOWN_ARROW) && p.x <= 590){
	// 	p.move(5,0);
	// }
	// if (keyIsDown(LEFT_ARROW) && !keyIsDown(RIGHT_ARROW) && keyIsDown(UP_ARROW) && keyIsDown(DOWN_ARROW) && p.x >= 10){
	// 	p.move(-5,0);
	// }
	// if (keyIsDown(UP_ARROW) && !keyIsDown(DOWN_ARROW) && keyIsDown(RIGHT_ARROW) && keyIsDown(LEFT_ARROW) && p.y >= 10){
	// 	p.move(0,-5);
	// }
	// if (keyIsDown(DOWN_ARROW) && !keyIsDown(UP_ARROW) && keyIsDown(RIGHT_ARROW) && keyIsDown(LEFT_ARROW) && p.y <= 590){
	// 	p.move(0,5);
    // }
