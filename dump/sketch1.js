var p = new Player();
var e = new Enemy();

var bulletData = [[10,135,1,0.05,-3,0.02],[5,45,2,0,0,0],[20,30,0.5,0.05,2,0.1]];

var bullet = new Array();
for (var i=0; i<bulletData.length; i++){
	bullet.push(new Bullet(bulletData[i][0],
						   bulletData[i][1],
						   bulletData[i][2],
						   bulletData[i][3],
						   bulletData[i][4],
						   bulletData[i][5],
						   bulletData[i][6]))
}

function Player(){
	this.x = 300;
	this.y = 450;
	this.show = function(){
		fill(255,127,0,255);
		ellipse(this.x,this.y,10,10);
	}
	this.move = function(dx,dy){
		this.x += dx;
		this.y += dy;
	}
	this.render = function(){
		translate(this.x,this.y);
	}
}

function Enemy(){
	this.x = 300;
	this.y = 300;
	this.show = function(){
		fill(255,0,0,255);
		ellipse(this.x,this.y,30,30);
	}
}

function Bullet(radius,
				theta,
				bulletSpeed,
				bulletAccel,
				angleSpeed,
				radiusIncrement){
	this.x = e.x;
	this.y = e.y;
	this.radius = radius;
	this.angle = theta;
	this.bulletSpeed = bulletSpeed;
	this.bulletAccel = bulletAccel;
	this.angleSpeed = angleSpeed;
	this.radiusIncrement = radiusIncrement;
	this.move = function(){
		if(this.x+radius/2 > 0 && this.x-radius/2 < 600
		   && this.y+radius/2 > 0 && this.y-radius/2 < 600){
			this.x += bulletSpeed*Math.cos(theta*Math.PI/180);
			this.y -= bulletSpeed*Math.sin(theta*Math.PI/180);
		} else {
			bulletSpeed = 0;
			bulletAccel = 0;
			angleSpeed = 0;
			radiusIncrement = 0;
		}
	}
	// this.init = function(bulletSpeed,theta){
	// 	this.x += speed*Math.cos(theta*Math.PI/180);
	// 	this.y -= speed*Math.sin(theta*Math.PI/180);
	// }
	this.changeRadius = function(){ //changing radius over time
		radius += radiusIncrement;
	}
	this.changeAngle = function(){
		theta += angleSpeed;
	}
	this.changeSpeed = function(newSpeed){ //changing speed
		bulletSpeed = newSpeed;
	}
	this.accelSpeed = function(){
		bulletSpeed += bulletAccel;
	}
	this.isHit = function(){
		return (Math.hypot(this.x-p.x,this.y-p.y)<2.5+radius/2
			    && Math.hypot(e.x-this.x,e.y-this.y)>15+radius/2
			    && this.x+radius > 0 && this.x-radius < 600
			    && this.y+radius > 0 && this.y-radius < 600);
	}
	this.show = function(){
		if(Math.hypot(e.x-this.x,e.y-this.y)>15+radius/2
		   && this.x+radius/2 > 0 && this.x-radius/2 < 600
		   && this.y+radius/2 > 0 && this.y-radius/2 < 600){
			fill(255);
			ellipse(this.x,this.y,radius,radius);
		}
	}
	this.render = function(){
		translate(this.x,this.y);
	}
}

function setX(x){
	this.x = x;
}
function setY(y){
	this.y = y;
}

function keyReleased(){
}

function keyPressed(){
}

function preload(){
	soundFormats("mp3");
	gengaozo = loadSound("Gengaozo/gengaozo.mp3");
}

function setup(){
	createCanvas(600,600);
	gengaozo.setVolume(0.1);
	gengaozo.play();
}

function draw(){ //68 = right, 65 = left, 87 = up, 83 = down
	if (keyIsDown(68) && !keyIsDown(65) && !keyIsDown(87) && !keyIsDown(83) && p.x <= 590){
		p.move(5,0); //right
	}
	if (keyIsDown(65) && !keyIsDown(68) && !keyIsDown(87) && !keyIsDown(83) && p.x >= 10){
		p.move(-5,0); //left
	}
	if (keyIsDown(87) && !keyIsDown(83) && !keyIsDown(68) && !keyIsDown(65) && p.y >= 10){
		p.move(0,-5); //up
	}
	if (keyIsDown(83) && !keyIsDown(87) && !keyIsDown(68) && !keyIsDown(65) && p.y <= 590){
		p.move(0,5); //down
	}
	if (keyIsDown(87) && keyIsDown(65) && !keyIsDown(83) && !keyIsDown(68) && p.y >= 10 && p.x >= 10){
		p.move(-5*Math.sqrt(2)/2,-5*Math.sqrt(2)/2); //upleft
	}
	if (keyIsDown(87) && keyIsDown(68) && !keyIsDown(83) && !keyIsDown(65) && p.y >= 10 && p.x <= 590){
		p.move(5*Math.sqrt(2)/2,-5*Math.sqrt(2)/2); //upright
	}
	if (keyIsDown(83) && keyIsDown(65) && !keyIsDown(87) && !keyIsDown(68) && p.y <= 590 && p.x >= 10){
		p.move(-5*Math.sqrt(2)/2,5*Math.sqrt(2)/2); //downleft
	}
	if (keyIsDown(83) && keyIsDown(68) && !keyIsDown(87) && !keyIsDown(65) && p.y <= 590 && p.x <= 590){
		p.move(5*Math.sqrt(2)/2,5*Math.sqrt(2)/2); //downright
	}
	if (keyIsDown(68) && !keyIsDown(65) && keyIsDown(87) && keyIsDown(83) && p.x <= 590){
		p.move(5,0);
	}
	if (keyIsDown(65) && !keyIsDown(68) && keyIsDown(87) && keyIsDown(83) && p.x >= 10){
		p.move(-5,0);
	}
	if (keyIsDown(87) && !keyIsDown(83) && keyIsDown(68) && keyIsDown(65) && p.y >= 10){
		p.move(0,-5);
	}
	if (keyIsDown(83) && !keyIsDown(87) && keyIsDown(68) && keyIsDown(65) && p.y <= 590){
		p.move(0,5);
	}
	background(0);
	p.show();
	bullet[0].show();
	bullet[1].show();
	bullet[2].show();
	e.show();
	p.render();
	bullet[0].render();
	bullet[1].render();
	bullet[2].render();
	bullet[0].move();
	bullet[0].accelSpeed();
	bullet[0].changeAngle();
	bullet[0].changeRadius();
	if(bullet[0].isHit()){
		console.log("Ouch");
	}
	bullet[1].move();
	if(bullet[1].isHit()){
		console.log("Ouchie");
	}
	bullet[2].move();
	bullet[2].accelSpeed();
	bullet[2].changeAngle();
	bullet[2].changeRadius();
	if(bullet[2].isHit()){
		console.log("Just Monika");
	}
}



//in function draw()
	// if (keyIsDown(RIGHT_ARROW) && !keyIsDown(LEFT_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(DOWN_ARROW) && p.x <= 590){
	// 	p.move(5,0);
	// }
	// if (keyIsDown(LEFT_ARROW) && !keyIsDown(RIGHT_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(DOWN_ARROW) && p.x >= 10){
	// 	p.move(-5,0);
	// }
	// if (keyIsDown(UP_ARROW) && !keyIsDown(DOWN_ARROW) && !keyIsDown(RIGHT_ARROW) && !keyIsDown(LEFT_ARROW) && p.y >= 10){
	// 	p.move(0,-5);
	// }
	// if (keyIsDown(DOWN_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(RIGHT_ARROW) && !keyIsDown(LEFT_ARROW) && p.y <= 590){
	// 	p.move(0,5);
	// }
	// if (keyIsDown(UP_ARROW) && keyIsDown(LEFT_ARROW) && !keyIsDown(DOWN_ARROW) && !keyIsDown(RIGHT_ARROW) && p.y >= 10 && p.x >= 10){
	// 	p.move(-5*Math.sqrt(2)/2,-5*Math.sqrt(2)/2);
	// }
	// if (keyIsDown(UP_ARROW) && keyIsDown(RIGHT_ARROW) && !keyIsDown(DOWN_ARROW) && !keyIsDown(LEFT_ARROW) && p.y >= 10 && p.x <= 590){
	// 	p.move(5*Math.sqrt(2)/2,-5*Math.sqrt(2)/2);
	// }
	// if (keyIsDown(DOWN_ARROW) && keyIsDown(LEFT_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(RIGHT_ARROW) && p.y <= 590 && p.x >= 10){
	// 	p.move(-5*Math.sqrt(2)/2,5*Math.sqrt(2)/2);
	// }
	// if (keyIsDown(DOWN_ARROW) && keyIsDown(RIGHT_ARROW) && !keyIsDown(UP_ARROW) && !keyIsDown(LEFT_ARROW) && p.y <= 590 && p.x <= 590){
	// 	p.move(5*Math.sqrt(2)/2,5*Math.sqrt(2)/2);
	// }
	// if (keyIsDown(RIGHT_ARROW) && !keyIsDown(LEFT_ARROW) && keyIsDown(UP_ARROW) && keyIsDown(DOWN_ARROW) && p.x <= 590){
	// 	p.move(5,0);
	// }
	// if (keyIsDown(LEFT_ARROW) && !keyIsDown(RIGHT_ARROW) && keyIsDown(UP_ARROW) && keyIsDown(DOWN_ARROW) && p.x >= 10){
	// 	p.move(-5,0);
	// }
	// if (keyIsDown(UP_ARROW) && !keyIsDown(DOWN_ARROW) && keyIsDown(RIGHT_ARROW) && keyIsDown(LEFT_ARROW) && p.y >= 10){
	// 	p.move(0,-5);
	// }
	// if (keyIsDown(DOWN_ARROW) && !keyIsDown(UP_ARROW) && keyIsDown(RIGHT_ARROW) && keyIsDown(LEFT_ARROW) && p.y <= 590){
	// 	p.move(0,5);
	// }


